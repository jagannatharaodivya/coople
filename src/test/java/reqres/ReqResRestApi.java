package reqres;


import io.restassured.RestAssured;
import io.restassured.http.Headers;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.junit.Test;
import java.io.File;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class ReqResRestApi {

    static Headers actual_header;

    //This class aims to test the Rest Api which is open source
    @Test()
    public void RestGetTest() {
        String endPoint1 = "api/Users/1";
        RestAssured.baseURI = "http://fakerestapi.azurewebsites.net/";
        RequestSpecification httpGet = RestAssured.given();

        ValidatableResponse responseGet = (ValidatableResponse) httpGet
                .header("Accept", "application/json")
                .queryParam("page", "2")
                .get(RestAssured.baseURI + endPoint1)
                .then()
                .assertThat().statusCode(200);
        responseGet.assertThat().body(matchesJsonSchemaInClasspath("user.json"));
    }

    @Test()
    public void RestPostTest() {
        String endpoint2 = "api/users";
        RestAssured.baseURI = "https://reqres.in/";
        RequestSpecification httpPost = RestAssured.given();
        File postUserData = new File("/Users/raodivya/Documents/TestTask/src/main/resources/postUser.json");
        ValidatableResponse responsePost = (ValidatableResponse) httpPost
                .headers("content-type", "application/json")
                .body(postUserData)
                .post(RestAssured.baseURI + endpoint2)
                .then()
                .assertThat()
                .statusCode(201);
    }


}
